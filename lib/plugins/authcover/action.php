<?php
if (!defined('DOKU_INC')) die();

// Use the actual cover cookies

require_once dirname(__FILE__) . '/cover_sessions.php';

class action_plugin_authcover extends DokuWiki_Action_Plugin
{
    /*
     * plugin should use this method to register its handlers with the dokuwiki's event controller
     */
    public function register(Doku_Event_Handler $controller) 
    {
        $controller->register_hook('ACTION_ACT_PREPROCESS','BEFORE', $this, 'handle_login');
        $controller->register_hook('FORM_LOGIN_OUTPUT', 'BEFORE', $this, 'handleLoginForm');
    }

    public function handle_login(&$event, $param)
    {
        global $ID;
        if ($event->data == 'login') {
            $url = \cover\session\get_session_url($this->getConf('cover_login_url'), DOKU_URL . wl($ID));
            header('Location: ' . $url , true, 302);
            die();
        }
    }

    public function handleLoginForm(Doku_Event $event)
    {
        global $ID;
        $form = $event->data;

        do {
            $form->removeElement(0);
        } while ($form->elementCount() > 0);

        $url = \cover\session\get_session_url($this->getConf('cover_login_url'), DOKU_URL . wl($ID));

        $form->addHTML(sprintf('<a class="btn btn-primary btn-lg" href="%s">Log in at Cover</a>', $url));
    }
}