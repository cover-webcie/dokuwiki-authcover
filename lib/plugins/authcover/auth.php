<?php
if (!defined('DOKU_INC')) die();

// Use the actual cover cookies

require_once dirname(__FILE__) . '/cover_sessions.php';

class auth_plugin_authcover extends DokuWiki_Auth_Plugin
{
	public function __construct()
	{
		parent::__construct();

		$this->cando['external'] = true;
		$this->cando['logout'] = true;

		$this->session_manager = new \cover\session\CoverSessionManager(
			$this->getConf('cover_app'),
			$this->getConf('cover_secret'),
			$this->getConf('cover_api_url'),
			$this->getConf('cover_cookie_name')
		);
	}

	public function trustExternal($user, $pass, $sticky = false)
	{
		global $USERINFO;

		$user_details = $this->session_manager->get_session();

		if ($user_details)
		{
			$_SERVER['REMOTE_USER'] = $user_details->id;

			$USERINFO['name'] = $this->_userFullName($user_details);
			$USERINFO['mail'] = $user_details->email;
			$USERINFO['grps'] = $this->_getCompatibleGroups();
			return true;
		}
	}

	private function _userFullName($details)
	{
		return $details->voornaam
			. (trim($details->tussenvoegsel)
				? ' ' . trim($details->tussenvoegsel)
				: '')
			. ' ' . $details->achternaam;
	}

	private function _getCompatibleGroups($user_id = null)
	{
		if (isset($user_id)) {
			$response = $this->session_manager->get_json('get_committees', ['member_id' => $user_id]);
			$committees = (array) $response->result;
		} else {
			$committees = $this->session_manager->get_committees();
		}

		// By default all members are member of the group user
		$groups = array('user');

		// Add all the committee's they're part of
		foreach ($committees as $login => $name)
			$groups[] = $login;

		// If the member is part of webcie, they're also admin.
		if (in_array('webcie', $groups))
			$groups[] = 'admin';

		return $groups;
	}

	public function getUserData($user_id, $requireGroups = true)
	{
		if (!ctype_digit((string) $user_id))
			return false;

		$response = $this->session_manager->get_json('get_member', ['member_id' => $user_id]);
		$details = $response->result;

		if (!$details)
			return false;

		if ($details->voornaam === null && $details->achternaam === null)
			$details->voornaam = '[Hidden]';

		$groups = [];

		if ($requireGroups) {
			$groups = $this->_getCompatibleGroups($user_id);
		}

		return array(
			'name' => $this->_userFullName($details),
			'mail' => $details->email,
			'grps' => $groups,
		);
	}

	public function logOff()
	{
		global $INPUT;
		
		$user_details = $this->session_manager->get_session();

		if ($user_details)
		{
			$page_url = ltrim($INPUT->server->str('REQUEST_URI'), '/');
			$url = \cover\session\get_session_url($this->getConf('cover_logout_url'), DOKU_URL . $page_url);
			header('Location: ' . $url , true, 302);
			die();
		}
	}
}
