<?php
/**
 * Default settings for the authcover plugin
 */

$conf['cover_app'] = '';
$conf['cover_secret'] = '';
$conf['cover_api_url'] = 'https://www.svcover.nl/api';
$conf['cover_login_url'] = 'https://www.svcover.nl/login';
$conf['cover_logout_url'] = 'https://www.svcover.nl/logout';
$conf['cover_cookie_name'] = 'cover_session_id';
