<?php
/**
 * Options for the authcover plugin
 */

$meta['cover_app']           = array('string');
$meta['cover_secret']        = array('string');
$meta['cover_api_url']       = array('string');
$meta['cover_login_url']     = array('string');
$meta['cover_logout_url']    = array('string');
$meta['cover_cookie_name']   = array('string');
