<?php
namespace cover\session;


class APIException extends \RuntimeException {}


/** Reverse of php's parse_url */
function _http_build_url($parts) { 
    return implode("", array(
        isset($parts['scheme']) ? $parts['scheme'] . '://' : '',
        isset($parts['user']) ? $parts['user'] : '',
        isset($parts['pass']) ? ':' . $parts['pass']  : '',
        (isset($parts['user']) || isset($parts['pass'])) ? "@" : '',
        isset($parts['host']) ? $parts['host'] : '',
        isset($parts['port']) ? ':' . intval($parts['port']) : '',
        isset($parts['path']) ? $parts['path'] : '',
        isset($parts['query']) ? '?' . $parts['query'] : '',
        isset($parts['fragment']) ? '#' . $parts['fragment'] : ''
    ));
}


/** Inject arguments into http url */
function _http_inject_url($url, array $data) {
    // Parse the url
    $url_parts = parse_url($url);

    // Explicitly parse the query part as well
    if (isset($url_parts['query']))
        parse_str($url_parts['query'], $url_query);
    else
        $url_query = array();

    // Splice in the token authentication
    $url_query = array_merge($data, $url_query);

    // Rebuild the url
    $url_parts['query'] = http_build_query($url_query, null, '&');
    return _http_build_url($url_parts);
}


/** Perform a signed http request */
function _http_signed_request($app, $secret, $url, array $post = null, $timeout=30) {
    $body = $post !== null ? http_build_query($post) : '';

    $checksum = sha1($body . $secret);

    $headers = "X-App: ". $app. "\r\n".
              "X-Hash: ". $checksum . "\r\n";

    if ($post !== null)
        $options = [
            'http' => [
                'header'  => $headers."Content-type: application/x-www-form-urlencoded\r\n",
                'timeout' => $timeout,
                'method'  => 'POST',
                'content' => $body
            ]
        ];
    else
        $options = [ 
            'http' => [
                'header'  => $headers,
                'timeout' => $timeout,
                'method'  => 'GET'
            ] 
        ];

    $context = stream_context_create($options);

    return file_get_contents($url, false, $context);
}


/** Create url to Cover Session management with return field */
function get_session_url($url, $next_url, $next_field='referrer') {
    return _http_inject_url($url, array($next_field => $next_url));
}


class CoverSessionManager
{
    public function __construct($app, $secret, $api_url, $cookie_name) {
        $this->app = $app;
        $this->secret = $secret;
        $this->api_url = $api_url;
        $this->cookie_name = $cookie_name;
    }

    /** Get JSON via a signed http request*/
    private function _http_get_json($url, array $data = null) {
        if ($data !== null)
            $url = _http_inject_url($url, $data);

        $response = _http_signed_request($this->app, $this->secret, $url);

        if (!$response)
            throw new APIException('No response');

        $data = json_decode($response);

        if ($data === null)
            throw new APIException('Response could not be parsed as JSON: <pre>' . htmlentities($response) . '</pre>');

        return $data;
    }

    /** Get Cover session data if member is logged in and login is valid*/
    public function get_session() {
        static $session = null;

        // Is there a cover website global session id available?
        if (!empty($_COOKIE[$this->cookie_name]))
            $session_id = $_COOKIE[$this->cookie_name];

        // If not, bail out. I have no place else to look :(
        else
            return null;

        if ($session !== null)
            return $session;

        $data = array(
            'method' => 'session_get_member',
            'session_id' => $session_id
            );

        $response = $this->_http_get_json($this->api_url, $data);

        return $session = !empty($response->result)
            ? $response->result
            : null;
    }

    /** Check if member is logged in and login is valid */
    public function logged_in() {
        return $this->get_session() !== null;
    }

    /** Get of which the logged in member is part */
    public function get_committees() {
        static $committees;

        if ($committees !== null)
            return $committees;
        
        $session = $this->get_session();

        if (!$session)
            return array();

        return $committees = (array) $this->get_session()->committees;
    }

    /** Check if logged in member is in a committee */
    public function session_in_committee($committee) {
        return in_array(strtolower($committee), $this->get_committees());
    }

    /** Get JSON from Cover API with session id */
    public function get_json($method, array $data=array(), $use_session=true) {
        if ($use_session && $this->logged_in() && !isset($data['session_id']))
            $data['session_id'] = $_COOKIE[$this->cookie_name];

        $data['method'] = $method;

        return $this->_http_get_json($this->api_url, $data);
    }
}
