<?php
/**
 * English language file for the authcover plugin settings
 */

$lang['cover_app'] = 'Cover App name';
$lang['cover_secret'] = 'Cover App secret';
$lang['cover_api_url'] = 'Base url of the Cover API';
$lang['cover_login_url'] = 'Login url of the Cover website';
$lang['cover_logout_url'] = 'Logout url of the Cover website';
$lang['cover_cookie_name'] = 'Cover session cookie name';
