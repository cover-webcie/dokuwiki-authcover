// Hide the search fields till the search button is clicked
jQuery(function() {
    jQuery('#dw__search').addClass('collapsed script');
    setTimeout(function() {jQuery('#dw__search').removeClass('script');}, 500);
    
    jQuery('#dw__search #qsearch__in').focus(function(e) {
        e.preventDefault();
        jQuery('#dw__search').removeClass('collapsed');
        jQuery('#dw__search #qsearch__in').get(0).focus();
    });

    jQuery('#dw__search #qsearch__in').focusout(function(e) {
        e.preventDefault();
        jQuery('#dw__search').addClass('collapsed immediate');
        setTimeout(function() {jQuery('#dw__search').removeClass('immediate');}, 500);
    });

    jQuery('#dw__search button[type="submit"]').click(function(evt) {
        if (!jQuery('#dw__search #qsearch__in').first().val())
            evt.preventDefault();

        jQuery('#dw__search').removeClass('collapsed');
        jQuery('#dw__search #qsearch__in').get(0).focus();
    });

    if (window.matchMedia('(max-width: 768px)').matches) {
        jQuery('#dw_toc .toc-contents.collapse').collapse('hide');
    }

    jQuery( window ).resize(function() {
        if (window.matchMedia('(max-width: 768px)').matches) {
            jQuery('#dw_toc .toc-contents.collapse').collapse('hide');
        } else {
            jQuery('#dw_toc .toc-contents.collapse').collapse('show');
        }
    });

    jQuery('#dw_toc .toc-contents.collapse').on('hide.bs.collapse', function () {
        var $icon = jQuery('#dw_toc i.fa');
        $icon.removeClass('fa-angle-up');
        $icon.addClass('fa-angle-down');
    });

    jQuery('#dw_toc .toc-contents.collapse').on('show.bs.collapse', function () {
        var $icon = jQuery('#dw_toc i.fa');
        $icon.removeClass('fa-angle-down');
        $icon.addClass('fa-angle-up');
    });
});
