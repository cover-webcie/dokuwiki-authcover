base     cover
author   Martijn Luinstra
email    martijn@svcover.nl
date     2018-08-21
name     Cover template
desc     DokuWiki template for the Cover wiki. Based on the DokuWiki Starter template. 
url      https://www.dokuwiki.org/template:starter
