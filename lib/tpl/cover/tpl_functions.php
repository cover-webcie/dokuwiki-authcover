<?php

/**
 * Template Functions
 *
 * This file provides template specific custom functions that are
 * not provided by the DokuWiki core.
 * It is common practice to start each function with an underscore
 * to make sure it won't interfere with future core functions.
 */

// must be run from within DokuWiki
if (!defined('DOKU_INC')) die();


/**
 * Custom function to render toc so we can override html rendering. Copied from core:
 *
 * Places the TOC where the function is called
 *
 * If you use this you most probably want to call tpl_content with
 * a false argument
 *
 * @author Andreas Gohr <andi@splitbrain.org>
 *
 * @param bool $return Should the TOC be returned instead to be printed?
 * @return string
 */

function _tpl_toc($return = false) {
    global $TOC;
    global $ACT;
    global $ID;
    global $REV;
    global $INFO;
    global $conf;
    global $INPUT;
    $toc = array();

    if(is_array($TOC)) {
        // if a TOC was prepared in global scope, always use it
        $toc = $TOC;
    } elseif(($ACT == 'show' || substr($ACT, 0, 6) == 'export') && !$REV && $INFO['exists']) {
        // get TOC from metadata, render if neccessary
        $meta = p_get_metadata($ID, '', METADATA_RENDER_USING_CACHE);
        if(isset($meta['internal']['toc'])) {
            $tocok = $meta['internal']['toc'];
        } else {
            $tocok = true;
        }
        $toc = isset($meta['description']['tableofcontents']) ? $meta['description']['tableofcontents'] : null;
        if(!$tocok || !is_array($toc) || !$conf['tocminheads'] || count($toc) < $conf['tocminheads']) {
            $toc = array();
        }
    } elseif($ACT == 'admin') {
        // try to load admin plugin TOC
        /** @var $plugin DokuWiki_Admin_Plugin */
        if ($plugin = plugin_getRequestAdminPlugin()) {
            $toc = $plugin->getTOC();
            $TOC = $toc; // avoid later rebuild
        }
    }

    \dokuwiki\Extension\Event::createAndTrigger('TPL_TOC_RENDER', $toc, null, false);
    $html = bootstrap_html_TOC($toc);
    if($return) return $html;
    echo $html;
    return '';
}

/**
 * Return the TOC rendered to Boostrap HTML
 */
function bootstrap_html_TOC($toc){
    if(!count($toc)) return '';
    global $lang;
    $out  = '<!-- TOC START -->'.DOKU_LF;
    $out .= '<div id="dw_toc" class="toc hidden-print">'.DOKU_LF;
    $out .= '<div class="hidden-print"><h3 class="toc-header"><a role="button" data-toggle="collapse" href="#dw_toc_contents" aria-expanded="true" aria-controls="collapseOne">';
    $out .= $lang['toc'];
    $out .= ' <i class="fa fa-angle-down" aria-hidden="true"></i></a></h3></div>'.DOKU_LF;
    $out .= '<div id="dw_toc_contents" class="toc-contents collapse in hidden-print">';
    $out .= html_buildlist($toc,'','html_list_toc','html_li_default',true);
    $out .= '</div>';
    $out .= '</div>'.DOKU_LF;
    $out .= '<!-- TOC END -->'.DOKU_LF;
    return $out;
}
