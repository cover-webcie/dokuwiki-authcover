<?php
/**
 * Template header, included in the main and detail files
 */

// must be run from within DokuWiki
if (!defined('DOKU_INC')) die();
?>

<header id="dokuwiki__header" class="page-header">
    <?php tpl_includeFile('header.html') ?>

    <?php if($conf['breadcrumbs']): ?>
        <div class="breadcrumbs"><?php tpl_breadcrumbs() ?></div>
    <?php endif ?>
    <?php if($conf['youarehere']): ?>
        <div class="breadcrumbs"><?php tpl_youarehere(' / ') // using en-spaces ?></div>
    <?php endif ?>
    <div class="tools">
        <h3 class="a11y sr-only"><?php echo $lang['page_tools'] ?></h3>
        <?php foreach ((new \dokuwiki\Menu\PageMenu())->getItems() as $item): ?>
            <?php if ($item->getType() === 'top') continue; ?>
            <a <?= buildAttributes($item->getLinkAttributes('btn btn-default btn-sm dw_')) ?>>
                <?= inlineSVG($item->getSvg()) ?>
                <span class="a11y sr-only"><?= $item->getLabel() ?></span>
            </a>
        <?php endforeach ?>
    </div>
</header>
