<?php
/**
 * Template header, included in the main and detail files
 */

// must be run from within DokuWiki
if (!defined('DOKU_INC')) die();
?>

<nav class="navbar navbar-cover">
    <div class="container-fluid">
        <div class="navbar-header">
            <button class="navbar-toggle collapsed navicon-button" href="#navbar-main-collapse" data-toggle="collapse" aria-expanded="false" aria-controls="navbar-main-collapse">
                <span class="navicon navicon-white"></span>
            </button>
            <a class="navbar-logo" href="//www.svcover.nl/"><img src="//cdl.svcover.nl/dist/v0.1/img/cover.png" alt="Cover"></a>
            <?php tpl_link(wl(),$conf['title'],'accesskey="h" title="[H]" class="navbar-brand"') ?>
        </div>
        <div class="collapse navbar-collapse" id="navbar-main-collapse">
            <ul class="navbar-right">
                <?php if(empty($_SERVER['REMOTE_USER'])): ?>
                    <li><?= (new dokuwiki\Menu\Item\Login())->asHtmlLink('dw_', false) ?></li>
                <?php else: ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <?= (new \dokuwiki\Menu\SiteMenu())->getListItems('dw_', false) ?>
                            <li role="separator" class="divider"></li>
                            <li class="dropdown-header"><?php tpl_userinfo(); ?></li>
                            <?= (new \dokuwiki\Menu\UserMenu())->getListItems('dw_', false) ?>
                        </ul>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
        <?php tpl_searchform(true, false) ?>
    </div>
</nav>
